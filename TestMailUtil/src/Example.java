
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Example {
	
	 public static void sendMail(String recepient) {
		 System.out.println("Preparing to send email");
	    	Properties properties = new Properties();
	    	
	    	properties.put("mail.smtp.auth", "true");
	    	properties.put("mail.smtp.starttls.enable", "true");
	    	properties.put("mail.smtp.host", "smtp.gmail.com");
	    	properties.put("mail.smtp.port", "587");
	    	
	    	String myAccountEmail = "souleakim5144@gmail.com";
	    	String password = "";
	    	
	    	Session session = Session.getInstance(properties, new Authenticator() {
	    		
	    		protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(myAccountEmail, password);
				}
			});
	    	
	    	Message message = prepareMessage(session, myAccountEmail, recepient);
			try {
				Transport.send(message);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Message sent successfully");
		}

	private static Message prepareMessage(Session session, String myAccountEmail, String recepient) {
		
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountEmail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			message.setSubject("My first email from Java App");
			message.setText("Hey there, \n looy my email !");
			return message;
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			Logger.getLogger(Example.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
	
	public static void main(String[] args) {
		sendMail("soule_akim@yahoo.fr");
	}

}
